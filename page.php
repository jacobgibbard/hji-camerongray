<?php while ( have_posts() ) : the_post(); ?>

	<header class="page-header">

            <?php the_title( '<h1 class="page-title">', '</h1>' ); ?>

    </header>

    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>        

        <?php edit_post_link(); ?>

        <section class="page-content">

            <?php the_content(); ?>

        </section>

        <footer>

        	<?php wp_link_pages(array('before' => '<nav class="pagination">', 'after' => '</nav>')); ?>
            
        </footer>

    </article>

<?php endwhile; ?>