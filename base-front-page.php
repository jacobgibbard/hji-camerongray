<!DOCTYPE html>

<html <?php language_attributes(); ?>>

    <?php get_header( hji_theme_template_base() ); ?>

    <body <?php body_class(); ?>>

        <!--[if lt IE 8]>
            <div class="alert alert-warning">
                <?php _e('You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.', 'hji-textdomain'); ?>
            </div>
        <![endif]-->

            <?php get_template_part( 'templates/topofthepage' ); ?>

            <div id="wrapper" class="body-wrapper">

            <?php do_action( 'hji_theme_before_navbar' ); ?>
            
            <?php get_template_part( 'templates/header-navbar' ); ?>
            
            <?php do_action( 'hji_theme_after_navbar' ); ?>

            <div class="blvd-slideshow"></div>

            <section class="map-wrap">
                    <div class="svg-wrap visible-lg visible-md" id="vectormap">                            
                        <img src="/wp-content/uploads/svg-mock.png" />
                    </div>
                    <div class="map-content">
                        <h3>Find Your Community</h3>
                        <div class="map-links">
                            <a href="/" class="arvada">Arvada</a>
                            <a href="/" class="brighton">Brighton</a>
                            <a href="/" class="broomfield">Broomfield</a>
                            <a href="/" class="commercecity">Commerce City</a>
                            <a href="/" class="denver">Denver</a>
                            <a href="/" class="erie">Erie</a>
                            <a href="/" class="golden">Golden</a>
                            <a href="/" class="henderson">Henderson</a>
                            <a href="/" class="lafayette">Lafayette</a>
                            <a href="/" class="lakewood">Lakewood</a>                            
                            <a href="/" class="lewisville">Louisville</a>
                            <a href="/" class="northglenn">Northglenn</a>
                            <a href="/" class="superior">Superior</a>
                            <a href="/" class="thornton">Thornton</a>
                            <a href="/" class="westminster">Westminster</a>
                            <a href="/" class="wheatridge">Wheat Ridge</a>
                        </div>
                    </div>
                </section>

            <section id="primary" class="primary-wrapper container homepagepush">

                <div class="row">

                    <?php do_action( 'hji_theme_before_content' ); ?>

                    <div id="content" class="<?php echo hji_theme_main_class(); ?>" role="main">

                        <?php do_action( 'hji_theme_before_content_col' ); ?>

                        <?php include hji_theme_template_path(); ?>

                        <?php do_action( 'hji_theme_after_content_col' ); ?>

                    </div>

                    <?php do_action( 'hji_theme_before_sidebar' ); ?>
                    
                    <?php ( hji_theme_display_sidebar() ? get_sidebar( hji_theme_template_base() ) : '' ) ?>

                    <?php do_action( 'hji_theme_after_sidebar' ); ?>

                </div>
            
            </section>            

            <?php do_action( 'hji_theme_after_primary' ); ?> 

            <?php if ( is_active_sidebar( 'blvd-upper-homewidgets' ) ) : ?>

                <section class="container upper-homewidgets-wrapper">

                    <div class="blvd-upper-homewidgets">
                        <?php dynamic_sidebar( 'blvd-upper-homewidgets'); ?>
                    </div>

                </section>

            <?php endif; ?>                     

            <section class="container">
                <?php get_template_part( 'templates/cta-boxes' ); ?> 
            </section>            
            
            <?php if ( is_active_sidebar( 'blvd-middle-homewidgets' ) ) : ?>

                <section class="container middle-homewidgets-wrapper">

                    <div class="blvd-middle-homewidgets">
                        <?php dynamic_sidebar( 'blvd-middle-homewidgets'); ?>
                    </div>

                </section>

            <?php endif; ?>                        

            <?php if ( is_active_sidebar( 'blvd-homepageparallax' ) ) : ?>

                    <div class="homepage-parallax">
                        <div class="container">
                            <div class="blvd-homepage-parallax row">
                                <?php dynamic_sidebar( 'blvd-homepageparallax'); ?>
                            </div>
                        </div>
                    </div>

            <?php endif; ?>

            <section class="container bottom-homewidgets-wrapper">

                <?php if ( is_active_sidebar( 'blvd-bottom-homewidgets' ) ) : ?>

                    <div class="blvd-bottom-homewidgets">
                        <?php dynamic_sidebar( 'blvd-bottom-homewidgets'); ?>
                    </div>

                <?php endif; ?>

            </section> 

            <section class="bottom-homepage-testimonials">
                <div class="container">
                    <h3>Testimonials</h3>
                    <?php echo do_shortcode('[show_testimonials]') ?>
                </div>
            </section>

            <?php get_footer( hji_theme_template_base() ); ?>

        </div>

    </body>

</html>